import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './index.css';
import Main from './main';
import Home from './home';

ReactDOM.render(
  <Router>
    <Switch>
      <Route path="/main">
        <Main/>
      </Route>
      <Route path="/">
        <Home/>
      </Route>
    </Switch>
  </Router>,
  document.getElementById('root'),
);


import React from "react";
import { Container, Typography, useTheme } from '@material-ui/core';
import { FlatList } from "react-native";
import im1 from './destiny.jpg';
import im2 from './music.jpg';
import im3 from './turntable.jpg';

function Home() {
    const theme = useTheme();
    const images = [im1, im2, im3]

    const createImage = (image) => {
        return (
            <img src={image.item.toString()} style={{
                flex: 1,
                width: '400px',
                height: '400px',
                objectFit: 'contain',
                marginTop: '0%',
                marginLeft: '2%',
                marginRight: '2%',
            }} />
        );
    };

    return (
        <main
            style={{
                display: 'flex',
                flexDirection: 'column',
                height: '100vh',
            }}
        >
            <Container
                maxWidth="sm"
                style={{
                    padding: theme.spacing(8, 0, 6),
                }}
            >
                <Typography
                    component="h1"
                    variant="h2"
                    align="center"
                    style={{ fontSize: '3rem', color: '#44cfbc' }}
                >
                    Bonne fête Papa!!!
                </Typography>
            </Container>
            <Container maxWidth="lg">
                <FlatList data={images} renderItem={image => createImage(image)} horizontal={true} />
            </Container>
            <Container
                maxWidth="md"
                style={{
                    marginTop: '2%',
                    borderTop: '2px solid #59cf7e',
                }}>
                <Typography
                    align="justify" style={{
                        display: 'block', marginLeft: '5%', marginBottom: '5%',
                    }}
                >
                    Bonne fête Papa! J'ai essayé d'être un peu plus orginial cette année, alors j'ai fait une carte
                    un peu différente! J'espère que tu vas aimer ton souper de ce soir et ta journée de fête, malgré la pandémie.
                    Comme toujours, je suis très reconnaissant de ce que tu fais pour moi tous les jours en m'aidant que ce soit
                    avec l'épicerie ou avec mes autres dépenses quand j'en ai besoin, mais aussi en étant toujours là pour moi. Merci
                    encore pour tout et j'espère que tu as profité du temps que tu étais le seul ingénieur dans la famille parce que
                    ça va changer bientôt ;)!
                </Typography>
            </Container>
        </main>
    )
}

export default Home;